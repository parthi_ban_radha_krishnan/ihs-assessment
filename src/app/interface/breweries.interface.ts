export interface Breweries {
    id: number,
    name: string,
    brewery_type: string,
    street: string,
    address_2 ?: string,
    address_3 ?: string,
    city: string,
    state: string,
    county_province ?: string,
    postal_code: string,
    country?: string,
    longitude?: string,
    latitude?: string,
    phone: string,
    website_url: string,
    updated_at?: string,
    created_at?: string
}

export interface Recipes {
    id: number,
    name: string,
    tagline: string,
    first_brewed: string,
    description: string
    image_url: string,
    ph: number,
    ingredients: Ingredients,
    food_pairing: string[],
    brewers_tips : string,
    contributed_by: string
}

export interface Ingredients{
    malt: SubIngredients[],
    hops: SubIngredients[],
    yeast: string
}

export interface SubIngredients {
    name: string,
    amount: object,
    add ?: string,
    attribute ?: string
}