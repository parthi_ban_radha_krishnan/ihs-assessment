import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Breweries, Recipes} from './../interface/breweries.interface';

@Injectable({
  providedIn: 'root'
})
export class BackendAPIsService {
  breweries:Breweries[] = [];
  recipes: Recipes[] = [];
  constructor(private http: HttpClient) { }

  /**
   * @returns breweries list
   */
  getBreweries(){
    return this.breweries;
  }

  /**
   * @param val list of breweries
   */
  setBreweries(val: Breweries[]){
    this.breweries = val;
  }

  /**
   * @returns recipes list
   */
  getRecipes(){
    return this.recipes;
  }

  /**
   * @param val list of recipes
   */
  setRecipes(val: Recipes[]){
    this.recipes = val;
  }

  /**
   * GET all breweries from the server
   */
  getAllBreweries(): Observable<Breweries[]> {
    return this.http.get<Breweries[]>('https://api.openbrewerydb.org/breweries');
  }

  /** 
   * GET all beer receipe from the server 
  */
  getAllBeerReceipe(): Observable <Recipes[]>{
    return this.http.get<Recipes[]>('https://api.punkapi.com/v2/beers');
  }

  /**
   * GET respective brewery info based selection
  */
  getBreweryInfo(id:number): Observable<Breweries>{
    return this.http.get<Breweries>(`https://api.openbrewerydb.org/breweries/${id}`);
  }
}
