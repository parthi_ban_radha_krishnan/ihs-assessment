import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AddBreweryComponent } from './components/add-brewery/add-brewery.component';

const routes: Routes = [
  {
    path: 'add',
    component: AddBreweryComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  { 
    path: '', 
    pathMatch: 'full', 
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
