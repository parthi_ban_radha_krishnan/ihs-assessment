import { Router } from '@angular/router';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BackendAPIsService } from '../../services/backend-apis.service';
import { Breweries, Recipes} from '../../interface/breweries.interface';
import { forkJoin } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  randomRecipe: any = null;
  breweryInfo: Breweries;
  oldRndId: number;
  @ViewChild('aboutBrewery') aboutBrewery: TemplateRef<any>;
  @ViewChild('errorModal') errorModal: TemplateRef<any>;  

  constructor(
    private router: Router,
    public breweryService: BackendAPIsService,
    private modalService: NgbModal
  ){
  }

  ngOnInit(){
    // Fetch list of breweries and recipes on load
    if(this.breweryService.breweries.length <=0 || this.breweryService.recipes.length <=0){
      forkJoin<Breweries[], Recipes[]>(
        this.breweryService.getAllBreweries(),
        this.breweryService.getAllBeerReceipe()
      ).subscribe(resp => {
        this.breweryService.setBreweries(resp[0]);
        this.breweryService.setRecipes(resp[1]);
      },
      (err)=>{
        this.modalService.open(this.errorModal);
      });
    }        
  }  

  /**
   * Fetch brewery info based on selection and show in modal
   * @param id
   */
  getMoreInfo(id:number){
    this.breweryService.getBreweryInfo(id).subscribe( info => {
      this.breweryInfo = info;
      console.log(info);
      this.modalService.open(this.aboutBrewery, { scrollable: true })
    },
    (err => {
      this.modalService.open(this.errorModal);
    }));
  }

  /** Generate random number between 1 and length of receipe list and 
      filter the respective receipe id from the array to show in the UI
      since we are not making api call for every button click 
  */
  getRandomBeerRecipe(){
    let  rndId = this.getRandomNumber();

    //Generate new random number again if the last generated number is same as the newly generated one 
    if(this.oldRndId === rndId){
      rndId = this.getRandomNumber();
    }
    this.oldRndId = rndId;
    this.randomRecipe = this.breweryService.getRecipes().filter(item => item.id === rndId)[0];
  }

  /**
   * Resuable function to generate random number
   * @returns number
   */
  getRandomNumber():number{
    return Math.floor(Math.random() * this.breweryService.getRecipes().length) + 1;
  }

  /** 
   * Remove brewery from the list 
   * @param id
  */
  removeBrewery(breweryId:number){
    this.breweryService.breweries = this.breweryService.getBreweries().filter(item => item.id !== breweryId);
  }

  /**
   * Open delete confirmation modaL
   * @param id 
   */
  openConfirmModal(id:number){
    const content = this.breweryService.getBreweries().filter(item => item.id == id)[0];
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.content = content;
    modalRef.result.then(
      (res) => {
        this.removeBrewery(id);
      }, 
      (err) => { console.log('Cancel/Close click')})
  }

  /**
   * Go to add brewery page
   */
  addBrewery(){
    this.router.navigateByUrl('/add');
  }
}