import { Component, OnInit, Input } from '@angular/core';
import { Breweries } from 'src/app/interface/breweries.interface';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BackendAPIsService } from 'src/app/services/backend-apis.service';

@Component({
  selector: 'add-brewery',
  templateUrl: './add-brewery.component.html',
  styleUrls: ['./add-brewery.component.css']
})
export class AddBreweryComponent implements OnInit {
  addBreweryForm: FormGroup;
  isSubmitted  =  false;
  constructor(
    private router: Router, 
    private formBuilder: FormBuilder, 
    public breweryService: BackendAPIsService ) { }

  ngOnInit(): void {
    //Initialize the form elements
    this.addBreweryForm  =  this.formBuilder.group({
      email: ['', Validators.required],
      type: ['', Validators.required],
      name: ['', Validators.required],
      address: ['', Validators.required],
      address2: [''],
      city: ['', Validators.required],
      state: ['', Validators.required],
      zip: ['', Validators.required],
      phone: ['',Validators.required]
    });
  }

  /**
   * 
   * Assign form element values in Breweries object and add it to the existing list in home page
   */
  add(){
    console.log(this.addBreweryForm.value);
    this.isSubmitted = true;
    if(this.addBreweryForm.invalid){
      return;
    }else{
      console.log(this.formControls);
      const newBrewery:Breweries = {
        id: Math.floor(100 + Math.random() * 900),
        brewery_type: this.formControls.type.value,
        name: this.formControls.name.value,
        street: this.formControls.address.value,
        city: this.formControls.city.value,
        state: this.formControls.state.value,
        country: 'United States',
        phone: this.formControls.phone.value,
        postal_code: this.formControls.zip.value,
        website_url: this.formControls.email.value,
        address_2: this.formControls.address2.value
      };
      this.breweryService.breweries.unshift(newBrewery);
      this.router.navigateByUrl('/home');
    }    
  }

  /**
   * Go to home page on click of Cancel button
   */
  goBack(){
    this.router.navigateByUrl('/home');
  }

  /**
   * Getter function for brewery form controls
   */
  get formControls() { return this.addBreweryForm.controls; }
}
