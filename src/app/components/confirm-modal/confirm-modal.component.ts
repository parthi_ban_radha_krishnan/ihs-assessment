import { Component, OnInit, Input } from '@angular/core';
import { Breweries } from 'src/app/interface/breweries.interface';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.css']
})
export class ConfirmModalComponent implements OnInit {
  @Input() content: Breweries;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void { }
}
